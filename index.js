
const express = require('express');
const app = express();
const ejs = require('ejs');
const path    = require("path");
const bodyParser = require('body-parser');
const product = require('./database');


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index');
})

app.get('/product', (req, res) => {
  res.json(product)
})

app.get('/product/:id',(req,res)=>{
res.json(product.find(product=>product.id === req.params.id))
})
	

app.post('/products', (req, res) => {
  product.push(req.body)
  res.status(200).json(product)
})


app.post('/product_delete', (req, res) => {
   const deletedIndex = product.findIndex(product => product.id === req.params.id)
   /*const deletedIndexN = product.findIndex(product => product.name === req.params.name)*/
   product.splice(deletedIndex, 1)
   res.status(200).json(product)
})


app.post('/product_update', (req, res) => {
  const updateIndex = product.findIndex(product => product.id === req.params.id)
  res.json(Object.assign(product[updateIndex], req.body))
res.status(200).json(product)
})

app.listen(3000, () => {
  console.log('Start server at port 3000.')
})